with Ada.Text_IO; use Ada.Text_IO;
with Ada.Integer_Text_IO; use Ada.Integer_Text_IO;
with Ada.Command_Line; use Ada.Command_Line;
with Ada.Numerics.Discrete_Random;

procedure Main is

   Dim : constant Integer := 1000000;
   Threads : Integer := Integer'Value(Argument(1));

   type Int_Array is array (1 .. Dim) of Integer;
   Arr : Int_Array;
   
   subtype Rand_Range is Integer range 1 .. 1000000;
   package Rand is new Ada.Numerics.Discrete_Random(Rand_Range); use Rand;
   Gen : Generator;
   procedure Init_Arr is
      Secret : Integer;
   begin
      reset(Gen);
      for I in 1 .. Dim loop
         Arr(I) := I;
      end loop;
      Secret := Random(Gen);
      Arr(Secret) := -1;
      Put_Line("Згенеровано масив з -1 за індексом: " & Secret'Img);
   end Init_Arr;

   function Part_Min(Start_Index, Finish_Index : Integer) return Integer is
      Min : Integer := Integer'Last;
   begin
      for I in Start_Index .. Finish_Index loop
         if Arr(I) < Min then
            Min := Arr(I);
         end if;
      end loop;
      return Min;
   end Part_Min;

   protected type Global_Min_Protector is
      procedure Set_Min(Value : Integer);
      function Get_Min return Integer;
   private
      Min : Integer := Integer'Last;
   end Global_Min_Protector;

   protected body Global_Min_Protector is
      procedure Set_Min(Value : Integer) is
      begin
         if Value < Min then
            Min := Value;
         end if;
      end Set_Min;

      function Get_Min return Integer is
      begin
         return Min;
      end Get_Min;
   end Global_Min_Protector;

   G_Min : Global_Min_Protector;

   protected type Task_Manager is
      procedure Initialize(Num_Threads : Integer);
      procedure Task_Completed;
      entry Wait_For_Completion;
   private
      Completed_Tasks : Integer := 0;
      Total_Tasks : Integer := 0;
   end Task_Manager;

   protected body Task_Manager is
      procedure Initialize(Num_Threads : Integer) is
      begin
         Total_Tasks := Num_Threads;
      end Initialize;

      procedure Task_Completed is
      begin
         Completed_Tasks := Completed_Tasks + 1;
      end Task_Completed;

      entry Wait_For_Completion when Completed_Tasks = Total_Tasks is
      begin
         null;
      end Wait_For_Completion;
   end Task_Manager;

   TM : Task_Manager;

   task type Min_Finder is
      entry Start(Start_Index, Finish_Index, Name : Integer);
   end Min_Finder;

   task body Min_Finder is
      Local_Min, Name : Integer;
      Start_Index, Finish_Index : Integer;
   begin
      accept Start(Start_Index, Finish_Index, Name : Integer) do
         Min_Finder.Start_Index := Start_Index;
         Min_Finder.Finish_Index := Finish_Index;
         Min_Finder.Name := Name;
      end Start;
      Local_Min := Part_Min(Start_Index, Finish_Index);
      Put_Line("Потік: [" & Name'Img & "] пройшов відрізок [" & Start_Index'Img & " - " & Finish_Index'Img & "] та знайшов мінімум: " & Local_Min'Img);
      G_Min.Set_Min(Local_Min);
      TM.Task_Completed;
   end Min_Finder;

   procedure Parallel_Min is
      Threads_Array : array(1 .. Threads) of Min_Finder;
      Segment_Size : Integer := Dim / Threads;
   begin
      TM.Initialize(Threads);
      for I in 1 .. Threads loop
         declare
            Start_Index : Integer := (I - 1) * Segment_Size + 1;
            Finish_Index : Integer := I * Segment_Size;
         begin
            if I = Threads then
               Finish_Index := Dim;
            end if;
            Threads_Array(I).Start(Start_Index, Finish_Index, I);
         end;
      end loop;

      TM.Wait_For_Completion;
      Put_Line("Глобальний мінімум: " & G_Min.Get_Min'Img);
   end Parallel_Min;

begin
   Init_Arr;
   Parallel_Min;
end Main;

